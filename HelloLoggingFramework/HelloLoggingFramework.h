//
//  HelloLoggingFramework.h
//  HelloLoggingFramework
//
//  Created by  sukdamrong kraewong on 20/12/2561 BE.
//  Copyright © 2561  sukdamrong kraewong. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HelloLoggingFramework.
FOUNDATION_EXPORT double HelloLoggingFrameworkVersionNumber;

//! Project version string for HelloLoggingFramework.
FOUNDATION_EXPORT const unsigned char HelloLoggingFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HelloLoggingFramework/PublicHeader.h>


#import <HelloLoggingFramework/HelloLogger.h>
