//
//  HelloLogger.m
//  HelloLoggingFramework
//
//  Created by  sukdamrong kraewong on 20/12/2561 BE.
//  Copyright © 2561  sukdamrong kraewong. All rights reserved.
//

#import "HelloLogger.h"

@implementation HelloLogger
- (void)helloWithText:(NSString *)text {
    NSLog(@"Hello, %@", text);
}
- (void)helloWithText2:(NSString *)text {
    NSLog(@"Hello, %@", text);
}
@end
