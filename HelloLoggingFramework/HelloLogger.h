//
//  HelloLogger.h
//  HelloLoggingFramework
//
//  Created by  sukdamrong kraewong on 20/12/2561 BE.
//  Copyright © 2561  sukdamrong kraewong. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelloLogger : NSObject
- (void)helloWithText:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
